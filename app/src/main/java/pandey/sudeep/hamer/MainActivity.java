package pandey.sudeep.hamer;

import android.app.FragmentManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity implements HaMeRFragment.ActivityCallback {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String TAG_TASK_FRAGMENT = "task_fragment";

    private int i =0;

    //private Handler handler = new Handler();




    private Button button;
    private TextView t1;

    private TextView end;

    private HaMeRFragment mTaskFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        t1 = findViewById(R.id.textView);
        end = findViewById(R.id.textView3);
        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // handler.handleMessage(Message.obtain());
                mTaskFragment.startThread();

            }
        });


        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        mTaskFragment = (HaMeRFragment) fm.findFragmentByTag(TAG_TASK_FRAGMENT);

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (mTaskFragment == null) {
            mTaskFragment = new HaMeRFragment();
            fm.beginTransaction().add(mTaskFragment, TAG_TASK_FRAGMENT).commit();
        }

        if(savedInstanceState!=null){
            int num = (int)savedInstanceState.get("val");
            t1.setText(Integer.toString(num));
        }




        //thread2 = new HandlerThread("SecondThread");
        //looper2 = thread2.getLooper();
        //handler2 = new Myhandler(looper2);
        //bundle2 = new Bundle();
        //bundle2.putFloat("float2",1);
        //message2 = Message.obtain();
        //message2.obj = bundle2;
        //message2.what = 2;




        Log.d(TAG, "onCreate() - Creating activity state..............");
        if(savedInstanceState!=null){
            Log.d(TAG, "onCreate() - Recreating activity state..............");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        Log.d(TAG, "onStart() - Starting activity..............");
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG, "onResume() - Resuming activity..............");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() - Pausing activity..............");
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("val",Integer.parseInt(t1.getText().toString()));

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() - Activity Stopped.....................");
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart() - Activity Restarted.....................");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() - Activity Destroyed.....................");
    }

    @Override
    public void updateTextView(){

        int start = Integer.parseInt(t1.getText().toString());
        start+=1;
        t1.setText(Integer.toString(start));
        Log.d(TAG, "MainActivity.updateTextView() - TextView updated...."+start+"....");


    }

    @Override
    public void updateStatus(){
        end.setText("Task Complete");
    }

}
