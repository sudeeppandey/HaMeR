package pandey.sudeep.hamer;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class HaMeRFragment extends Fragment {

    private ActivityCallback callback;

    private MyHandlerThread myHandlerThread;
    private Handler UIHandler = new Handler(Looper.getMainLooper());

    private Runnable runnable;



    private static final String TAG = HaMeRFragment.class.getSimpleName();


    interface ActivityCallback {
        void updateTextView();

        void updateStatus();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (ActivityCallback) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        myHandlerThread = new MyHandlerThread("myHandlerThread");
        runnable = new Runnable() {
            @Override
            public void run() {
                int update =0;
                for (int i=0;i<100;i++){
                    try {
                        Thread.sleep(100);
                        update+=1;
                        //Message msg = Message.obtain();
                        //msg.arg1=update;
                        while(callback==null){
                            continue;

                        }
                        //UIHandler.sendMessage(msg);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.updateTextView();
                            }
                        });
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    }
                }
                //Message endMsg = Message.obtain();
                //endMsg.arg1=1000;
                //UIHandler.sendMessage(endMsg);
                while(callback==null){
                    continue;
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.updateStatus();
                    }
                });
            }
        };

        }

        @Override
        public void onDetach () {
            super.onDetach();
            callback = null;
        }

        @Override
        public void onDestroy(){
            super.onDestroy();
            myHandlerThread.quit();
            Log.d(TAG, "onDestroy() - Fragment Destroyed BANG BANG!!!!!!!.....................");

        }

        public void startThread () {

            myHandlerThread.start();
            myHandlerThread.prepareHandler();
            myHandlerThread.postTask(runnable);
        }

        public class MyHandlerThread extends HandlerThread {

            private Handler handler;

            public MyHandlerThread(String name) {
                super(name);
            }

            public void postTask(Runnable task) {
                handler.post(task);
            }

            public void prepareHandler() {
                handler = new Handler(getLooper());
            }
        }
    }

