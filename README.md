# Project Background

Background Execution techniques like **AsyncTask** and **HandlerThread** don't
survive configuration changes like screen rotation. Both of these entities
are associated with the **Activity** component that define and start them.
When the starting component passes through ***onDestroy()*** during 
configuration change, both **AsyncTask** as well as **HandlerThread** die.

This code sample demonstrates how **FRAGMENT** could be used to preserve 
**AsyncTask** and **HandlerThread** during screen rotation.

### Code Review

**FRAGMENT** is composed that implements a callback interface which can update
**Activity** class' UI. When *setRetainInstance(true)* method is called on
fragment, it gets retained during configuration changes. Fragment defines
the HandlerThread or AsyncTask. It's in activity class where these entities 
are invoked. These background tasks are directly associated with fragment
instance. Communication with activity UI happens via callback interface from
the fragment itself. When configuration change occurs, activity instance dies.
Fragment retains itself and so do handlerthread or asynctask. When new activity
instance is created, the same fragment instance attaches itself to the new 
activity instance. The background task never gets interrupted and communication
with activity UI continues via the same callback interface.
